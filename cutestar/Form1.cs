using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Media;
using System.Runtime.InteropServices;

namespace Cutestar_Ticket_Examiner
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal IDP;//票号
            decimal IDU;//最高票号
            decimal IDD;//最低票号
            string ID2;//票号转文本
            string str1;//检票记录
            decimal CNT;//检票总次数
            string LCNT;//计数记录
            decimal RCNT;//入场人数
            string LRCNT;//入场人数记录
            LCNT = File.ReadAllText(@"星萌CNT.txt");
            LRCNT = File.ReadAllText(@"星萌RCNT.txt");
            CNT = Convert.ToDecimal(LCNT);
            RCNT = Convert.ToDecimal(LRCNT);
            ID2 = "0" + Convert.ToString(textBox3.Text);//前面加个0，防止崩溃
            IDP = Convert.ToDecimal(ID2);
            IDD = Convert.ToDecimal("0" + textBox1.Text);
            IDU = Convert.ToDecimal("0" + textBox2.Text);
            /* 票号转数值 */
            textBox3.Text = "";
            /* 清空输入栏 */
            CNT = CNT + 1;
            label6.Text = "0" + Convert.ToString(CNT);
            File.WriteAllText(@"星萌CNT.txt", Convert.ToString(CNT));//写入数据库
            if (IDP > IDU)
            {
                label4.Text = "核验失败，票号错误";
                SoundPlayer player = new SoundPlayer();
                player.SoundLocation = @"2.wav";
                player.Load(); //同步加载声音
                player.Play(); //启用新线程播放
                               //player.PlayLooping(); //循环播放模式
                               //player.PlaySync(); //UI线程同步播放
            }
            else if (IDP < IDD)
            {
                label4.Text = "核验失败，票号错误";
                SoundPlayer player = new SoundPlayer();
                player.SoundLocation = @"2.wav";
                player.Load(); //同步加载声音
                player.Play(); //启用新线程播放
                               //player.PlayLooping(); //循环播放模式
                               //player.PlaySync(); //UI线程同步播放
            }
            else//票号在区间内，进入查重判定
            {
                W:
                if (File.Exists(@"星萌.txt"))//换成C盘，因为真的有人电脑没！D！盘！(更新：直接换软件安装目录了)
                {
                    str1 = File.ReadAllText(@"星萌.txt");//读取数据库并装入内存
                    int c = str1.IndexOf(ID2);
                    if (c >= 0)
                    {
                        label4.Text = "核验成功，重复进场";
                        SoundPlayer player = new SoundPlayer();
                        player.SoundLocation = @"3.wav";
                        player.Load(); //同步加载声音
                        player.Play(); //启用新线程播放
                                       //player.PlayLooping(); //循环播放模式
                                       //player.PlaySync(); //UI线程同步播放
                    }
                    else
                    {
                        string str2 = (str1 + ID2 + "（" + DateTime.Now.ToLongTimeString().ToString()+ ")" + System.Environment.NewLine);//将本次票号与原数据库拼合
                        File.WriteAllText(@"星萌.txt", str2);//写入数据库
                        RCNT = RCNT + 1;
                        label6.Text = "0" + Convert.ToString(CNT);
                        File.WriteAllText(@"星萌RCNT.txt", Convert.ToString(RCNT));//写入数据库
                        label8.Text = "0" + Convert.ToString(RCNT);
                        label4.Text = "核验成功，请进场";
                        SoundPlayer player = new SoundPlayer();
                        player.SoundLocation = @"1.wav";
                        player.Load(); //同步加载声音
                        player.Play(); //启用新线程播放
                        //player.PlayLooping(); //循环播放模式
                         //player.PlaySync(); //UI线程同步播放
                    }
                }
                else
                {
                    File.WriteAllText(@"星萌.txt", "");//没数据库就现场造一个
                    goto W;
                }
            }
              
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(@"星萌CNT.txt"))
            {
                
            }
            else
            {
                File.WriteAllText(@"星萌CNT.txt", "0000");//没计数就现场造一个
            }
            if (System.IO.File.Exists(@"星萌RCNT.txt"))
            {

            }
            else
            {
                File.WriteAllText(@"星萌RCNT.txt", "0000");//没计数就现场造一个
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string v_OpenFolderPath = @"星萌.txt";
            System.Diagnostics.Process.Start("explorer.exe", v_OpenFolderPath);//弹窗打开数据库，未创建的话打开我的文档（雾）

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "本检票姬适用于顺序票号检票" + System.Environment.NewLine +
                "输入最大和最小票号即可" + System.Environment.NewLine +
                "此处提一下吾爱破解论坛#www.52pojie.cn#" + System.Environment.NewLine +
                "另外，支持跳码检票与列表检票的新版正在制作中" + System.Environment.NewLine +
                "也将在52pojie发布（等我学会用access数据库吧）" + System.Environment.NewLine +
                "原版为易语言编写，今后的版本将使用C#等" + System.Environment.NewLine +
                "多种编程语言编写，边学边玩吧！"
                , "制作：萌萌哒miku", MessageBoxButtons.OK);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            decimal RCNT;//入场人数
            string LRCNT;//入场人数记录
            LRCNT = File.ReadAllText(@"星萌RCNT.txt");
            RCNT = Convert.ToDecimal(LRCNT);
            RCNT = RCNT + 1;
            label8.Text = "0" + Convert.ToString(RCNT);
            File.WriteAllText(@"星萌RCNT.txt", Convert.ToString(RCNT));//写入数据库
        }
    }
}
