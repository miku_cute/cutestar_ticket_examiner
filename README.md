# [星萌检票姬](https://gitee.com/miku_cute/cutestar_ticket_examiner)

#### 介绍
一个为了自己的漫展而制作的开源检票软件。简单好用。
最新版本请看各种分支，曾经固定下来的 release 版代码扔在 release_xxx 分支处。
不会用 git，master 提交过几次之后就不知道怎么改了。
等以后使用的人多了，希望各位使用此系统的主办方能给作者免个票o(=·ω·=)m
当前属于咕咕咕状态。请直接下载release即可。
作者社团 Blog：[萌星球动漫社](https://www.tianyiclub.xyz)

#### 软件架构
不知怎么写，利用NAS似乎能做到多机台并行检票？


#### 安装教程

1. 下载
2. 解压
3. 开用！

#### 使用说明

1. 插上一个条码枪
2. 扫描门票上的条码
3. 获得检票结果

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)[]()